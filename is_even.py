def isEven(val):
    val = bin(val)
    return val.endswith('0')
