'''FIFO_list - кастомный класс, 2й класс - collections.deque.'''
from collections import deque

class FIFO_list():
    def __init__(self, length):
        ''':param length: Длина кольцевого буфера.'''
        self.list = []
        self.length = length
    def enqueue(self, val):
        '''Добавить значение в буфер.'''
        if len(self.list) == self.length:
            self.list.pop()
        self.list.insert(0, val)
    def dequeue(self):
        '''Вытащить самое раннее добавленное значение.'''
        return self.list.pop()
    def isEmpty(self):
        '''Проверить буфер на пустосту.'''
        return len(self.list) == 0
    def front(self):
        '''Получить самое раннее добавленное значение.'''
        return self.list[-1]
    def rear(self):
        '''Получить самое позднее добавленное значение.'''
        return self.list[0]
